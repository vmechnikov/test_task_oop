<?php
require_once('classes/Drink.php');
foreach (glob('classes/*.php') as $file) {

    $class = basename($file, '.php');

    if($class == 'Drink')
        continue;

    require_once $file;

    if(class_exists($class)) {
        $drink = new $class;
        require('view/drink.php');
    }
}