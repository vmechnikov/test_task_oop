<?php

class Cappuccino extends Drink {
    function __construct() {
        $this->title = 'Cappuccino';
        $this->description = 'Italian';
        $this->consist = array(
            'water' => 0.1,
            'coffee' => 0.5,
            'milk' => 0.4,
            'sugar' => 0.1
        );
        $this->set_price();
    }
}