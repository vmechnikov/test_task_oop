<?php

class Coffee extends Drink {
    function __construct() {
        $this->title = 'Coffee';
        $this->description = 'Brazilian';
        $this->consist = array(
            'water' => 0.1,
            'coffee' => 0.5,
            'sugar' => 0.1
        );
        $this->set_price();
    }
}