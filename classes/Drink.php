<?php

abstract class Drink {
    public $title;
    public $description;
    public $price;
    public $consist;

    protected function set_price() {
        foreach($this->consist as $el_price) {
            $this->price += $el_price;
        }
    }
}