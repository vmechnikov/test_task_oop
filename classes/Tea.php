<?php

class Tea extends Drink {
    function __construct() {
        $this->title = 'Tea';
        $this->description = 'Ceylon';
        $this->consist = array(
            'water' => 0.1,
            'tea_bag' => 0.3,
            'sugar' => 0.1
        );
        $this->set_price();
    }
}